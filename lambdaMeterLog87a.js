var AWS = require('aws-sdk');
s3 = new AWS.S3({
    apiVersion: '2006-03-01'
});
var dynamodb = new AWS.DynamoDB({region:'us-east-2'});
var docClient = new AWS.DynamoDB.DocumentClient({region:'us-east-2'});
exports.handler = (event, context) => {
   var mybucket = '0013a2004155087a';
           
            s3 = new AWS.S3({
                apiVersion: '2006-03-01',
                params: {
                    Bucket: mybucket
                },
            });
            var files = [];
            var ChildFolders = [];
            var subSubFolders = [];
            var subFolders = [];
            var folders = [];
            var mydate;
            var myfolder = 'Meter Information';
            s3.listObjects({
                Delimiter: '/'
            }, function (err1, data1) {
                if (err1) {
                    return alert('There was an error listing your folders: ' + err1.message);
                } else {
                    folders = data1.CommonPrefixes.map(function (commonPrefix) {
                        var prefix = commonPrefix.Prefix;
                        var folderName = decodeURIComponent(prefix.replace('/', ''));
                        return folderName;
                    });

                   
                }
                for(var j=0; j< folders.length; j++)
                {
                    //var j = 0;
                
                    s3.listObjects({
                        Delimiter: '/',
                        Prefix: folders[j] + '/'
                    }, function (err2, data2) {
                        if (err2) {
                            return alert('There was an error listing your subfolders: ' + err2.message);
                        } else {
                            subFolders = data2.CommonPrefixes.map(function (commonPrefix) {
                                var prefix1 = commonPrefix.Prefix;
                                var folderName1 = decodeURIComponent(prefix1.replace('', ''));
                                return folderName1;
                            });
                        }

                
                        for(var k = 0;k < subFolders.length; k++)
                        {
                            //var k = 0;
                            
                            s3.listObjects({
                                Delimiter: '/',
                                Prefix: subFolders[k]
                            }, function (err3, data3) {
                                if (err3) {
                                    return alert('There was an error listing your subsubfolders: ' + err3.message);
                                } else {
                                    subSubFolders = data3.CommonPrefixes.map(function (commonPrefix) {
                                        var prefix3 = commonPrefix.Prefix;
                                        var folderName3 = decodeURIComponent(prefix3.replace('', ''));
                                        return folderName3;
                                    });
                                }
                                //for(var l=0; l< subSubFolders.length; l++)
                                {
                                    var l = 2;
                                    
                                    s3.listObjects({
                                        Delimiter: '/',
                                        Prefix: subSubFolders[l]
                                    }, function (err4, data4) {
                                        if (err4) {
                                            return alert('There was an error listing your childfolders: ' + err4.message);
                                        } else {
                                            
                                            files = data4.Contents.map(function (photo) {
                                                var photoKey = photo.Key;
                                                
                                                return photoKey;
                                            });
                                        }
                                        
                                        var prearr = [];
                                        var curerr = [];
                                        for (var m = 0; m < files.length; m++)
                                        { 
                                            var fileKey = files[m];

                                            if (fileKey != 'undefined' && fileKey != '' && fileKey != null) {
                                                
                                                if (fileKey.split('/')[2] == myfolder) {
                                                    var getKey = fileKey.slice(-23, -4).split(' ');

                                                    var params = {
                                                        Bucket: mybucket,
                                                        Key: fileKey,
                                                    };

                                                    s3.getObject(params, function (err5, data5) {
                                                        if (err5) {
                                                            console.log(err5, err5.stack);

                                                        } else {
                                                            
                                                            var mycon = data5.Body.toString('ascii');


                                                            

                                                            if (mycon.includes('\n')) {
                                                                var test = mycon.split('\n');

                                                                var loop = test.length / 9;
                                                                var k = 1;
                                                                
                                                                var dt = '';
                                                                dt += '{';
                                                                for (var p = 0; p < Math.round(loop) - 1; p++) {
                                                                    if (prearr.length < Math.round(loop) - 1) {
                                                                        prearr[p] = (test[k + 6]) ? test[k + 6].split(':')[1].trim().split(' ')[0].trim() : 0;
                                                                    } else {
                                                                        curerr[p] = (test[k + 6]) ? test[k + 6].split(':')[1].trim().split(' ')[0].trim() : 0;
                                                                    }

                                                                    dt += '"' + test[k + 3].slice(-1) + '":{';
                                                                    dt += '"' + test[k + 3].split(':')[0] + '":"' + test[k + 3].split(':')[1] + '",';
                                                                    dt += '"' + test[k + 4].split(':')[0] + '":"' + test[k + 4].split(':')[1] + '",';
                                                                    dt += '"' + test[k + 5].split(':')[0] + '":"' + test[k + 5].split(':')[1] + '",';
                                                                    dt += '"' + test[k + 6].split(':')[0] + '":"' + test[k + 6].split(':')[1] + '",';
                                                                    dt += '"' + test[k + 7].split(':')[0] + '":"' + test[k + 7].split(':')[1] + '",';
                                                                    dt += '"' + test[k + 8].split(':')[0] + '":{';
                                                                    dt += '"' + ((test[k + 9]) ? (test[k + 9].split(':')[0]).trim() : 0) + '":"' + ((test[k + 9].split(':')[1]) ? (test[k + 9].split(':')[1]).trim() : 0) + '",';
                                                                    dt += '"' + ((test[k + 10]) ? (test[k + 10].split(':')[0]).trim() : 0) + '":"' + ((test[k + 10].split(':')[1]) ? (test[k + 10].split(':')[1]).trim() : 0) + '",';
                                                                    dt += '"' + ((test[k + 11]) ? (test[k + 11].split(':')[0]).trim() : 0) + '":"' + ((test[k + 11].split(':')[1]) ? (test[k + 11].split(':')[1]).trim() : 0) + '"';
                                                                    dt += '}';
                                                                    if (p == Math.round(loop) - 2)
                                                                        dt += '}';
                                                                    else
                                                                        dt += '},';

                                                                    k += 10;
                                                                }
                                                                dt += '}';
                                                                

                                                                var is_same = (prearr.length == curerr.length) && prearr.every(function (element, index) {
                                                                    return element === curerr[index];
                                                                });

                                                                if(prearr.length == Math.round(loop) - 1 && curerr.length == 0) {
                                                                    mlog();
                                                                    
                                                                }

                                                                if (!is_same) {
                                                                    
                                                                    mlog();
                                                                    prearr = curerr;
                                                                }
                                                                
                                                                function mlog() {
                                                                    var params = {
                                                                        TableName: "MeterLog",
                                                                        Item: {
                                                                            "Date": getKey[0],
                                                                            "Time": getKey[1],
                                                                            "PumpID": mybucket + '_' + fileKey.split('/')[0].slice(-1),
                                                                            "MeterLogKey": mybucket + '_' + fileKey.split('/')[0].slice(-1) + '_' + getKey[0] + '_' + getKey[1],
                                                                            //"LastModified": data.LastModified,
                                                                            "info": dt,
                                                                            "ValveType": test[2].split(':')[1].trim()

                                                                        }
                                                                    };
                                                                    docClient.put(params, function (err, data) {
                                                                        if (err) {
                                                                            
                                                                            console.log("Unable to add item: " + "\n" + JSON.stringify(err, undefined, 2));
                                                                        } else {
                                                                            
                                                                            console.log("PutItem succeeded: " + "\n" + JSON.stringify(data, undefined, 2));
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }

            });
};