import json
import sys
import logging
import pymysql
import os
database = os.environ['database'];
rdshost = os.environ['rdshost'];
username = os.environ['username'];
password = os.environ['password'];

class Utils:
    def __init__(self):
        pass

    @staticmethod
    def parse_parameters(event):
            message = event['Records'][0]['Sns']['Message']
            subject = event['Records'][0]['Sns']['Subject']
            
            
            messagearray = message.split(';\n')
            return_parameters = {}
            return_parameters['Pump_ID'] = 4
            return_parameters['ConfigurationKey'] = subject
            return_parameters['version'] = messagearray[0].split(",")[2]
            return_parameters['UnitType'] = messagearray[1].split(",")[2]
            return_parameters['Cash_Grade1'] = messagearray[4].split(",")[2]
            return_parameters['Cash_Grade2'] = messagearray[5].split(",")[2]
            return_parameters['Cash_Grade3'] = messagearray[6].split(",")[2]
            return_parameters['Cash_Grade4'] = messagearray[7].split(",")[2]
            return_parameters['Cash_Grade5'] = messagearray[8].split(",")[2]
            return_parameters['Cash_Grade6'] = messagearray[9].split(",")[2]
            return_parameters['Credit_Grade1'] = messagearray[10].split(",")[2]
            return_parameters['Credit_Grade2'] = messagearray[11].split(",")[2]
            return_parameters['Credit_Grade3'] = messagearray[12].split(",")[2]
            return_parameters['Credit_Grade4'] = messagearray[13].split(",")[2]
            return_parameters['Credit_Grade5'] = messagearray[14].split(",")[2]
            return_parameters['Credit_Grade6'] = messagearray[15].split(",")[2]
            return_parameters['Calibration_Meter1'] = messagearray[16].split(",")[2]
            return_parameters['Calibration_Meter5'] = messagearray[17].split(",")[2]
            return_parameters['Calibration_Meter4'] = messagearray[18].split(",")[2]
            return_parameters['Calibration_Meter8'] = messagearray[19].split(",")[2]
            return_parameters['Blend_Grade1'] = messagearray[20].split(",")[2]
            return_parameters['Blend_Grade2'] = messagearray[21].split(",")[2]
            return_parameters['Blend_Grade3'] = messagearray[22].split(",")[2]
            return_parameters['Blend_Grade4'] = messagearray[23].split(",")[2]
            return_parameters['Blend_Grade5'] = messagearray[24].split(",")[2]
            return_parameters['Blend_Grade6'] = messagearray[25].split(",")[2]
            return_parameters['Flow_Grade1'] = messagearray[26].split(",")[2]
            return_parameters['Flow_Grade2'] = messagearray[27].split(",")[2]
            return_parameters['Flow_Grade3'] = messagearray[28].split(",")[2]
            return_parameters['Flow_Grade4'] = messagearray[29].split(",")[2]
            return_parameters['Flow_Grade5'] = messagearray[30].split(",")[2]
            return_parameters['Flow_Grade6'] = messagearray[31].split(",")[2]
            return_parameters['Unit_of_Measure'] = messagearray[33].split(",")[2]
            return_parameters['Preset_Type'] = messagearray[34].split(",")[2]
            return_parameters['Vapor_Vac'] = messagearray[35].split(",")[2]
            return_parameters['Push_To_Start'] = messagearray[36].split(",")[2]
            return_parameters['Push_To_Stop'] = messagearray[37].split(",")[2]
            return_parameters['ATC'] = messagearray[38].split(",")[2]
            return_parameters['Totalizer'] = messagearray[39].split(",")[2]
            return_parameters['Nozzle_Activated'] = messagearray[40].split(",")[2]
            
            return {"parsedParams": return_parameters, "err": None}
        
class AuroraProvider:
    def __init__(self):

    
        try:
            self.conn = pymysql.connect(rdshost, user=username, passwd=password, db=database, connect_timeout=5)
        except:
            print("ERROR: Unexpected error: Could not connect to MySql instance.")
            sys.exit()

        print("SUCCESS: Connection to RDS mysql instance succeeded")
    
    def insert(self, result_params):
        print(result_params)
        with self.conn.cursor() as cur:
             
            cur.execute("""insert Configuration(Pump_ID, ConfigurationKey, Date, Version, UnitType, Cash_Grade1, Cash_Grade2, Cash_Grade3, Cash_Grade4, Cash_Grade5, Cash_Grade6, Credit_Grade1, Credit_Grade2, Credit_Grade3, Credit_Grade4, Credit_Grade5, Credit_Grade6, Calibration_Meter1, Calibration_Meter5, Calibration_Meter4, Calibration_Meter8, Blend_Grade1, Blend_Grade2, Blend_Grade3, Blend_Grade4, Blend_Grade5, Blend_Grade6, Flow_Grade1, Flow_Grade2, Flow_Grade3, Flow_Grade4, Flow_Grade5, Flow_Grade6, Unit_of_Measure, Preset_Type, Vapor_Vac, Push_To_Start, Push_To_Stop, ATC, Totalizer, Nozzle_Activated)
                values('%d', '%s', CURDATE(), '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')""" % (result_params['Pump_ID'], result_params['ConfigurationKey'], result_params['version'], result_params['UnitType'], result_params['Cash_Grade1'], result_params['Cash_Grade2'], result_params['Cash_Grade3'], result_params['Cash_Grade4'], result_params['Cash_Grade5'], result_params['Cash_Grade6'], result_params['Credit_Grade1'], result_params['Credit_Grade2'], result_params['Credit_Grade3'], result_params['Credit_Grade4'], result_params['Credit_Grade5'], result_params['Credit_Grade6'], result_params['Calibration_Meter1'], result_params['Calibration_Meter5'], result_params['Calibration_Meter4'], result_params['Calibration_Meter8'], result_params['Blend_Grade1'], result_params['Blend_Grade2'], result_params['Blend_Grade3'], result_params['Blend_Grade4'], result_params['Blend_Grade5'], result_params['Blend_Grade6'], result_params['Flow_Grade1'], result_params['Flow_Grade2'], result_params['Flow_Grade3'], result_params['Flow_Grade4'], result_params['Flow_Grade5'], result_params['Flow_Grade6'], result_params['Unit_of_Measure'], result_params['Preset_Type'], result_params['Vapor_Vac'], result_params['Push_To_Start'], result_params['Push_To_Stop'], result_params['ATC'], result_params['Totalizer'], result_params['Nozzle_Activated']))
            print ("""insert Configuration(Pump_ID, ConfigurationKey, Date, Version, UnitType, Cash_Grade1, Cash_Grade2, Cash_Grade3, Cash_Grade4, Cash_Grade5, Cash_Grade6, Credit_Grade1, Credit_Grade2, Credit_Grade3, Credit_Grade4, Credit_Grade5, Credit_Grade6, Calibration_Meter1, Calibration_Meter5, Calibration_Meter4, Calibration_Meter8, Blend_Grade1, Blend_Grade2, Blend_Grade3, Blend_Grade4, Blend_Grade5, Blend_Grade6, Flow_Grade1, Flow_Grade2, Flow_Grade3, Flow_Grade4, Flow_Grade5, Flow_Grade6, Unit_of_Measure, Preset_Type, Vapor_Vac, Push_To_Start, Push_To_Stop, ATC, Totalizer, Nozzle_Activated) 
            values('%d', '%s', CURDATE(), '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')""" % (result_params['Pump_ID'], result_params['ConfigurationKey'], result_params['version'], result_params['UnitType'], result_params['Cash_Grade1'], result_params['Cash_Grade2'], result_params['Cash_Grade3'], result_params['Cash_Grade4'], result_params['Cash_Grade5'], result_params['Cash_Grade6'], result_params['Credit_Grade1'], result_params['Credit_Grade2'], result_params['Credit_Grade3'], result_params['Credit_Grade4'], result_params['Credit_Grade5'], result_params['Credit_Grade6'], result_params['Calibration_Meter1'], result_params['Calibration_Meter5'], result_params['Calibration_Meter4'], result_params['Calibration_Meter8'], result_params['Blend_Grade1'], result_params['Blend_Grade2'], result_params['Blend_Grade3'], result_params['Blend_Grade4'], result_params['Blend_Grade5'], result_params['Blend_Grade6'], result_params['Flow_Grade1'], result_params['Flow_Grade2'], result_params['Flow_Grade3'], result_params['Flow_Grade4'], result_params['Flow_Grade5'], result_params['Flow_Grade6'], result_params['Unit_of_Measure'], result_params['Preset_Type'], result_params['Vapor_Vac'], result_params['Push_To_Start'], result_params['Push_To_Stop'], result_params['ATC'], result_params['Totalizer'], result_params['Nozzle_Activated']))
                
            self.conn.commit()
        

def lambda_handler(event, context):
    table_name = 'Configuration'
    repo=AuroraProvider()
    result_params = Utils.parse_parameters(event)
    repo.insert(result_params=result_params['parsedParams'])
    print('suceessfully inserted')
    return 'Inserted records'
