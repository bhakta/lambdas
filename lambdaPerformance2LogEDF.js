var AWS = require('aws-sdk');
s3 = new AWS.S3({
    apiVersion: '2006-03-01'
});
var lambda = new AWS.Lambda();
var dynamodb = new AWS.DynamoDB({region: 'us-east-2'});
var docClient = new AWS.DynamoDB.DocumentClient({region: 'us-east-2'});
exports.handler = (event, context, callback) => {
    mybucket = '0013A200415B9EDF';
    
    s3 = new AWS.S3({
        apiVersion: '2006-03-01',
        params: {
            Bucket: mybucket
        },
    });
    var files = [];
    var ChildFolders = [];
    var subSubFolders = [];
    var subFolders = [];
    var folders = [];
    var mydate;
    var myfolder = 'Performance Report 2';
            s3.listObjects({
                Delimiter: '/'
            }, function (err1, data1) {
                if (err1) {
                    return console.log('There was an error listing your folders: ' + err1.message);
                } else {
                    folders = data1.CommonPrefixes.map(function (commonPrefix) {
                        var prefix = commonPrefix.Prefix;
                        var folderName = decodeURIComponent(prefix.replace('/', ''));
                        return folderName;
                    });

                    
                }
                

               for(var j = 0; j < folders.length; j++)
                { 
                    //var j = 0;
                    
                    s3.listObjects({
                        Delimiter: '/',
                        Prefix: folders[j] + '/'
                    }, function (err2, data2) {
                        if (err2) {
                            return console.log('There was an error listing your subfolders: ' + err2.message);
                        } else {
                            subFolders = data2.CommonPrefixes.map(function (commonPrefix) {
                                var prefix1 = commonPrefix.Prefix;
                                var folderName1 = decodeURIComponent(prefix1.replace('', ''));
                                return folderName1;
                            });
                        }

                        

                        for (var k = 0; k < subFolders.length; k++) {
                            
                            s3.listObjects({
                                Delimiter: '/',
                                Prefix: subFolders[k]
                            }, function (err3, data3) {
                                if (err3) {
                                    return console.log('There was an error listing your subsubfolders: ' + err3.message);
                                } else {
                                    subSubFolders = data3.CommonPrefixes.map(function (commonPrefix) {
                                        var prefix3 = commonPrefix.Prefix;
                                        var folderName3 = decodeURIComponent(prefix3.replace('', ''));
                                        return folderName3;
                                    });
                                }
                                //for(var l=0; l<subSubFolders.length; l++)
                                {
                                    var l = 4;
                                    
                                    s3.listObjects({
                                        Delimiter: '/',
                                        Prefix: subSubFolders[l]
                                    }, function (err4, data4) {
                                        if (err4) {
                                            return console.log('There was an error listing your childfolders: ' + err4.message);
                                        } else {
                                            
                                            files = data4.Contents.map(function (photo) {
                                                var photoKey = photo.Key;
                                                
                                                return photoKey;
                                            });
                                        }
                                        
                                        var prearr = [];
                                        var curerr = [];
                                        for (var m = 0; m < files.length; m++) { //var m=0; 
                                            var fileKey = files[m];

                                            
                                            if (fileKey != 'undefined' && fileKey != '' && fileKey != null) {
                                                
                                                if (fileKey.split('/')[2] == myfolder) {
                                                    var getKey = fileKey.slice(-23, -4).split(' ');

                                                    var params = {
                                                        Bucket: mybucket,
                                                        Key: fileKey,
                                                    };

                                                    s3.getObject(params, function (err5, data5) {
                                                        if (err5) {
                                                            console.log(err5, err5.stack); // an error occurred

                                                        } else {
                                                            
                                                            var mycon = data5.Body.toString('ascii');
                                                           
                                                            if (mycon.includes(';\n')) {
                                                                var test = mycon.split(';\n')
                                                                

                                                                if (prearr.length == 0) {
                                                                    for (var ck = 0; ck < 15; ck++) {
                                                                        if (ck != 6 && ck != 7 && ck != 8 && ck != 12 && ck != 13)
                                                                            prearr[ck] = (test[ck + 1]) ? test[ck + 1].split(",")[2].trim() : 0;
                                                                    }
                                                                    p1log();
                                                                } else {
                                                                    for (var ck = 0; ck < 15; ck++) {
                                                                        if (ck != 6 && ck != 7 && ck != 8 && ck != 12 && ck != 13)
                                                                            curerr[ck] = (test[ck + 1]) ? test[ck + 1].split(",")[2].trim() : 0;
                                                                    }
                                                                }

                                                                var is_same = (prearr.length == curerr.length) && prearr.every(function (element, index) {
                                                                    return element === curerr[index];
                                                                });


                                                                if (!is_same) {
                                                                    
                                                                    p1log();
                                                                    prearr = curerr;
                                                                }

                                                                function p1log() {
                                                                    var params = {
                                                                        TableName: "Performance2Log",
                                                                        Item: {
                                                                            "Date": getKey[0],
                                                                            "Time": getKey[1],
                                                                            "PumpID": mybucket + '_' + fileKey.split('/')[0].slice(-1),
                                                                            "Perform2LogKey": mybucket + '_' + fileKey.split('/')[0].slice(-1) + '_' + getKey[0] + '_' + getKey[1],
                                                                            
                                                                            "Flow_Grade1": (test[1]) ? test[1].split(",")[2].trim() : 0,
                                                                            "Flow_Grade2": (test[2]) ? test[2].split(",")[2].trim() : 0,
                                                                            "Flow_Grade3": (test[3]) ? test[3].split(",")[2].trim() : 0,
                                                                            "Flow_Grade4": (test[4]) ? test[4].split(",")[2].trim() : 0,
                                                                            "Flow_Grade5": (test[5]) ? test[5].split(",")[2].trim() : 0,
                                                                            "Flow_Grade6": (test[6]) ? test[6].split(",")[2].trim() : 0,
                                                                            "Two_Wire_Count": (test[9]) ? test[9].split(",")[2].trim() : 0,
                                                                            "StandAlone_Count": (test[10]) ? test[10].split(",")[2].trim() : 0,
                                                                            "Preset_Overruns": (test[11]) ? test[11].split(",")[2].trim() : 0,
                                                                            "Preset_Underruns": (test[12]) ? test[12].split(",")[2].trim() : 0,
                                                                            "Error_Count": (test[15]) ? test[15].split(",")[2].trim() : 0
                                                                        }
                                                                    };
                                                                    docClient.put(params, function (err, data) {
                                                                        if (err) {
                                                                           
                                                                            console.log("Unable to add item: " + "\n" + JSON.stringify(err, undefined, 2));
                                                                        } else {
                                                                           
                                                                            console.log("PutItem succeeded: " + "\n" + JSON.stringify(data, undefined, 2));
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }

            });
    
};