var AWS = require('aws-sdk');
s3 = new AWS.S3({
    apiVersion: '2006-03-01'
});
var dynamodb = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient();
exports.handler = function(event,context) {
const uuidv1 = require('uuid/v1');
uuidv1();
mybucket = '0013A200415B9EDF';
s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {
        Bucket: mybucket,
        },
});
var query = 'Configuration Report';
s3.listObjects(function (err1, data1) {
    if (err1) {
        return console.log('There was an error listing your folders: ' + err1.message);
    } else {
        
        var newdata = data1.Contents.filter(dt => dt.Key.toLowerCase().indexOf(query.toLowerCase()) > -1);


        
        var mydate = new Date(Math.max.apply(null, newdata.map(function (e) {
            return new Date(e.LastModified);
        })));

        var findata = newdata.filter(dq => String(dq.LastModified) == String(mydate));
        

        findata.forEach(function (n) {
            var fileKey = n.Key;
            
            var getKey = fileKey.slice(-23, -4).split(' ');

            var params = {
                Bucket: mybucket,
                Key: fileKey,
            };
            
            s3.getObject(params, function (err5, data5) {
                
                if (err5) {
                    console.log(err5, err5.stack); 

                } 
                else  {
                
                var mycon = data5.Body.toString('ascii');
            
            if(mycon.includes(';\n'))
                {
                    var test = mycon.split(';\n')
                    
                    var params = {
                        TableName :"Configuration",
                        Item:{
                            "Date": getKey[0],
                            "Time": getKey[1],
                            "PumpID": mybucket+'_'+fileKey.split('/')[0].slice(-1),
                            "ConfigurationKey": mybucket+'_'+fileKey.split('/')[0].slice(-1)+'_'+getKey[0]+'_'+getKey[1],
                            "Version": test[0],
                            "UnitType": test[1],
                            "Cash_Grade1": (test[4])?test[4].split(",")[2]:0,
                            "Cash_Grade2": (test[5])?test[5].split(",")[2]:0,
                            "Cash_Grade3": (test[6])?test[6].split(",")[2]:0,
                            "Cash_Grade4": (test[7])?test[7].split(",")[2]:0,
                            "Cash_Grade5": (test[8])?test[8].split(",")[2]:0,
                            "Cash_Grade6": (test[9])?test[9].split(",")[2]:0,
                            "Credit_Grade1": (test[10])?test[10].split(",")[2]:0,
                            "Credit_Grade2": (test[11])?test[11].split(",")[2]:0,
                            "Credit_Grade3": (test[12])?test[12].split(",")[2]:0,
                            "Credit_Grade4": (test[13])?test[13].split(",")[2]:0,
                            "Credit_Grade5": (test[14])?test[14].split(",")[2]:0,
                            "Credit_Grade6": (test[15])?test[15].split(",")[2]:0,
                            "Calibration_Meter1": (test[16])?test[16].split(",")[2]:0,
                            "Calibration_Meter5": (test[17])?test[17].split(",")[2]:0,
                            "Calibration_Meter4": (test[18])?test[18].split(",")[2]:0,
                            "Calibration_Meter8": (test[19])?test[19].split(",")[2]:0,
                            "Blend_Grade1": (test[20])?test[20].split(",")[2]:0,
                            "Blend_Grade2": (test[21])?test[21].split(",")[2]:0,
                            "Blend_Grade3": (test[22])?test[22].split(",")[2]:0,
                            "Blend_Grade4": (test[23])?test[23].split(",")[2]:0,
                            "Blend_Grade5": (test[24])?test[24].split(",")[2]:0,
                            "Blend_Grade6": (test[25])?test[25].split(",")[2]:0,
                            "Flow_Grade1": (test[26])?test[26].split(",")[2]:0,
                            "Flow_Grade2": (test[27])?test[27].split(",")[2]:0,
                            "Flow_Grade3": (test[28])?test[28].split(",")[2]:0,
                            "Flow_Grade4": (test[29])?test[29].split(",")[2]:0,
                            "Flow_Grade5": (test[30])?test[30].split(",")[2]:0,
                            "Flow_Grade6": (test[31])?test[31].split(",")[2]:0,
                            "Unit_of_Measure": (test[33])?test[33].split(",")[2]:0,
                            "Preset_Type": (test[34])?test[34].split(",")[2]:0,
                            "Vapor_Vac": (test[35])?test[35].split(",")[2]:0,
                            "Push_To_Start": (test[36])?test[36].split(",")[2]:0,
                            "Push_To_Stop": (test[37])?test[37].split(",")[2]:0,
                            "ATC": (test[38])?test[38].split(",")[2]:0,
                            "Totalizer": (test[39])?test[39].split(",")[2]:0,
                            "Nozzle_Activated": (test[40])?test[40].split(",")[2]:0,
                        
                        }
                    };
                    docClient.put(params, function(err, data) {
                        if (err) {
                            
                            console.log("Unable to add item: " + "\n" + JSON.stringify(err, undefined, 2));
                        } else {
                            
                            console.log("PutItem succeeded: " + "\n" + JSON.stringify(data, undefined, 2));
                        }
                    });
                }
               }
            });
        });
    }
});

};