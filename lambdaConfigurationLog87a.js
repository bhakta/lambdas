var AWS = require('aws-sdk');
s3 = new AWS.S3({
    apiVersion: '2006-03-01'
});
var lambda = new AWS.Lambda();
var dynamodb = new AWS.DynamoDB({region:'us-east-2'});
var docClient = new AWS.DynamoDB.DocumentClient({region:'us-east-2'});
exports.handler = (event, context, callback) => {
    var mybucket = '0013a2004155087a';
    s3 = new AWS.S3({
        apiVersion: '2006-03-01',
        params: {
            Bucket: mybucket
        },
    });
    var files = [];
    var ChildFolders = [];
    var subSubFolders = [];
    var subFolders = [];
    var folders = [];
    var mydate;
    var myfolder = 'Configuration Report';
    s3.listObjects({
        Delimiter: '/'
    }, function (err1, data1) {
        if (err1) {
            return alert('There was an error listing your folders: ' + err1.message);
        } else {
            folders = data1.CommonPrefixes.map(function (commonPrefix) {
                var prefix = commonPrefix.Prefix;
                var folderName = decodeURIComponent(prefix.replace('/', ''));
                return folderName;
            });
        }
        for(var j=0; j < folders.length; j++)
        {
            //var j = 0;
            
            s3.listObjects({
                Delimiter: '/',
                Prefix: folders[j] + '/'
            }, function (err2, data2) {
                if (err2) {
                    return console.log('There was an error listing your subfolders: ' + err2.message);
                } else {
                    subFolders = data2.CommonPrefixes.map(function (commonPrefix) {
                        var prefix1 = commonPrefix.Prefix;
                        var folderName1 = decodeURIComponent(prefix1.replace('', ''));
                        return folderName1;
                    });
                }

                
                //for(var k = subFolders.length-1; k >= 0; k--)
                for(var k = 0;k < subFolders.length; k++)
                {
                    //var k=0;
                    s3.listObjects({
                        Delimiter: '/',
                        Prefix: subFolders[k]
                    }, function (err3, data3) {
                        if (err3) {
                            return console.log('There was an error listing your subsubfolders: ' + err3.message);
                        } else {
                            subSubFolders = data3.CommonPrefixes.map(function (commonPrefix) {
                                var prefix3 = commonPrefix.Prefix;
                                var folderName3 = decodeURIComponent(prefix3.replace('', ''));
                                return folderName3;
                            });
                        }
                        //for(var l=0; l< subSubFolders.length; l++)
                        {
                            var l = 0;
                            
                            s3.listObjects({
                                Delimiter: '/',
                                Prefix: subSubFolders[l]
                            }, function (err4, data4) {
                                if (err4) {
                                    return console.log('There was an error listing your childfolders: ' + err4.message);
                                } else {
                                    
                                    files = data4.Contents.map(function (photo) {
                                        var photoKey = photo.Key;
                                        
                                        return photoKey;
                                    });
                                }
                                
                                var prearr = [];
                                var curerr = [];
                                for (var m = 0; m < files.length; m++) 
                                { 
                                    
                                    var fileKey = files[m];

                                    if (fileKey !== 'undefined' && fileKey !== '' && fileKey !== null) {
                                        
                                        if (fileKey.split('/')[2] == myfolder && fileKey.slice(-3) == 'txt') {
                                            
                                            var getKey = fileKey.slice(-23, -4).split(' ');

                                            var params = {
                                                Bucket: mybucket,
                                                Key: fileKey,
                                            };

                                            s3.getObject(params, function (err5, data5) {
                                                if (err5) {
                                                    console.log(err5, err5.stack);

                                                } else {
                                                    
                                                    var mycon = data5.Body.toString('ascii');
                                                    
                                                    if (mycon.includes(';\n')) {
                                                        var test = mycon.split(';\n');
                                                        
                                                       
                                                        if (prearr.length == 0) 
                                                        {
                                                            for(var ck=0; ck< 28; ck++)
                                                            {
                                                                if((ck+4) != 16 && (ck+4) != 17 && (ck+4) != 18 && (ck+4) != 19) {   
                                                                    prearr[ck] = (test[ck+4])?test[ck+4].split(",")[2].trim():0;    
                                                                }
                                                            }                                                                        
                                                            clog();
                                                        } else {
                                                            for(var ck=0; ck< 28; ck++)
                                                            {
                                                                if((ck+4) != 16 && (ck+4) != 17 && (ck+4) != 18 && (ck+4) != 19) {   
                                                                    curerr[ck] = (test[ck+4])?test[ck+4].split(",")[2].trim():0;    
                                                                }
                                                            }         
                                                        }

                                                        var is_same = (prearr.length == curerr.length) && prearr.every(function(element, index) {
                                                            return element === curerr[index]; 
                                                        });
                                                       
                                                        
                                                        if(!is_same)                                                                
                                                        {
                                                            clog();
                                                            prearr = curerr;
                                                        }


                                                        function clog() {
                                                            var params = {
                                                                TableName: "ConfigurationLog",
                                                                Item: {
                                                                    "Date": getKey[0],
                                                                    "Time": getKey[1],
                                                                    "PumpID": mybucket + '_' + fileKey.split('/')[0].slice(-1),
                                                                    "ConfigLogKey": mybucket + '_' + fileKey.split('/')[0].slice(-1) + '_' + getKey[0] + '_' + getKey[1],
                                                                    
                                                                    "Version": test[0],
                                                                    "UnitType": test[1],
                                                                    "Cash_Grade1": (test[4]) ? test[4].split(",")[2] : 0,
                                                                    "Cash_Grade2": (test[5]) ? test[5].split(",")[2] : 0,
                                                                    "Cash_Grade3": (test[6]) ? test[6].split(",")[2] : 0,
                                                                    "Cash_Grade4": (test[7]) ? test[7].split(",")[2] : 0,
                                                                    "Cash_Grade5": (test[8]) ? test[8].split(",")[2] : 0,
                                                                    "Cash_Grade6": (test[9]) ? test[9].split(",")[2] : 0,
                                                                    "Credit_Grade1": (test[10]) ? test[10].split(",")[2] : 0,
                                                                    "Credit_Grade2": (test[11]) ? test[11].split(",")[2] : 0,
                                                                    "Credit_Grade3": (test[12]) ? test[12].split(",")[2] : 0,
                                                                    "Credit_Grade4": (test[13]) ? test[13].split(",")[2] : 0,
                                                                    "Credit_Grade5": (test[14]) ? test[14].split(",")[2] : 0,
                                                                    "Credit_Grade6": (test[15]) ? test[15].split(",")[2] : 0,
                                                                    "Calibration_Meter1": (test[16]) ? test[16].split(",")[2] : 0,
                                                                    "Calibration_Meter5": (test[17]) ? test[17].split(",")[2] : 0,
                                                                    "Calibration_Meter4": (test[18]) ? test[18].split(",")[2] : 0,
                                                                    "Calibration_Meter8": (test[19]) ? test[19].split(",")[2] : 0,
                                                                    "Blend_Grade1": (test[20]) ? test[20].split(",")[2] : 0,
                                                                    "Blend_Grade2": (test[21]) ? test[21].split(",")[2] : 0,
                                                                    "Blend_Grade3": (test[22]) ? test[22].split(",")[2] : 0,
                                                                    "Blend_Grade4": (test[23]) ? test[23].split(",")[2] : 0,
                                                                    "Blend_Grade5": (test[24]) ? test[24].split(",")[2] : 0,
                                                                    "Blend_Grade6": (test[25]) ? test[25].split(",")[2] : 0,
                                                                    "Flow_Grade1": (test[26]) ? test[26].split(",")[2] : 0,
                                                                    "Flow_Grade2": (test[27]) ? test[27].split(",")[2] : 0,
                                                                    "Flow_Grade3": (test[28]) ? test[28].split(",")[2] : 0,
                                                                    "Flow_Grade4": (test[29]) ? test[29].split(",")[2] : 0,
                                                                    "Flow_Grade5": (test[30]) ? test[30].split(",")[2] : 0,
                                                                    "Flow_Grade6": (test[31]) ? test[31].split(",")[2] : 0,
                                                                    "Unit_of_Measure": (test[33]) ? test[33].split(",")[2] : 0,
                                                                    "Preset_Type": (test[34]) ? test[34].split(",")[2] : 0,
                                                                    "Vapor_Vac": (test[35]) ? test[35].split(",")[2] : 0,
                                                                    "Push_To_Start": (test[36]) ? test[36].split(",")[2] : 0,
                                                                    "Push_To_Stop": (test[37]) ? test[37].split(",")[2] : 0,
                                                                    "ATC": (test[38]) ? test[38].split(",")[2] : 0,
                                                                    "Totalizer": (test[39]) ? test[39].split(",")[2] : 0,
                                                                    "Nozzle_Activated": (test[40]) ? test[40].split(",")[2] : 0,

                                                                }
                                                            };
                                                            docClient.put(params, function(err, data) {
                                                                if (err) {
                                                                    
                                                                    console.log("Unable to add item: " + "\n" + JSON.stringify(err, undefined, 2));
                                                                } else {
                                                                    
                                                                    console.log("PutItem succeeded: " + "\n" + JSON.stringify(data, undefined, 2));
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }

    });

};