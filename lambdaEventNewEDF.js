var AWS = require('aws-sdk');

s3 = new AWS.S3({
    apiVersion: '2006-03-01'
});

var dynamodb = new AWS.DynamoDB({region:'us-east-2'});
var docClient = new AWS.DynamoDB.DocumentClient({region:'us-east-2'});

exports.handler = (event, context) => {

const uuidv1 = require('uuid/v1');
uuidv1();


mybucket = '0013A200415B9EDF';


s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {
        Bucket: mybucket
    },
});

var files = [];
var ChildFolders = [];
var subSubFolders = [];
var subFolders = [];
var folders = [];
var mydate;
var mydevice = 'Device';
var myfolder = 'Event Log';
s3.listObjects({
    Delimiter: '/'
}, function (err1, data1) {
    if (err1) {
        return alert('There was an error listing your folders: ' + err1.message);
    } else {
        folders = data1.CommonPrefixes.map(function (commonPrefix) {
            var prefix = commonPrefix.Prefix;
            var folderName = decodeURIComponent(prefix.replace('/', ''));
            return folderName;
        });
        folders = folders.filter(function (e) {
            return e.indexOf(mydevice) !== -1;
        });
        
    }
    

    for(var j = 0; j < folders.length; j++)
    {
        
        s3.listObjects({
            Delimiter: '/',
            Prefix: folders[j] + '/'
        }, function (err2, data2) {
            if (err2) {
                return alert('There was an error listing your subfolders: ' + err2.message);
            } else {
                subFolders = data2.CommonPrefixes.map(function (commonPrefix) {
                    var prefix1 = commonPrefix.Prefix;
                    var folderName1 = decodeURIComponent(prefix1.replace('', ''));
                    return folderName1;
                });
            }

           
            //for(var k = subFolders.length-1;k >= subFolders.length-3; k--)
            //for(var k = subFolders.length-1;k >= 0; k--)
            {
                var k = subFolders.length - 1;
               
                s3.listObjects({
                    Delimiter: '/',
                    Prefix: subFolders[k]
                }, function (err3, data3) {
                    if (err3) {
                        return alert('There was an error listing your subsubfolders: ' + err3.message);
                    } else {
                        subSubFolders = data3.CommonPrefixes.map(function (commonPrefix) {
                            var prefix3 = commonPrefix.Prefix;
                            var folderName3 = decodeURIComponent(prefix3.replace('', ''));
                            return folderName3;
                        });

                        subSubFolders = subSubFolders.filter(function (e) {
                            return e.indexOf(myfolder) !== -1;
                        });
                    }
                   

                    //for(var l = 0;l < subSubFolders.length; l++)
                    {
                        var l = 0;
                        s3.listObjects({
                            Delimiter: '/',
                            Prefix: subSubFolders[l]
                        }, function (err4, data4) {
                            if (err4) {
                                return alert('There was an error listing your childfolders: ' + err4.message);
                            } else {
                               
                                files = data4.Contents.map(function (photo) {
                                    var photoKey = photo.Key;
                                    
                                    return photoKey;
                                });
                            }
                            

                            for(var m=0; m < files.length; m++)
                            {
                                
                                var fileKey = files[m];

                                
                                if (fileKey != 'undefined' && fileKey != '' && fileKey != null) {
                                    
                                    if (fileKey.split('/')[2] == myfolder && fileKey.slice(-3) == 'txt') {
                                        var getKey = fileKey.slice(-23, -4).split(' ');

                                        var para = {
                                            TableName: "EventNew",
                                            KeyConditionExpression: "#I = :Number",
                                            FilterExpression: "#D = :d and #T = :t",
                                            ExpressionAttributeNames: {
                                                "#I": "PumpID",
                                                "#D": "Date",
                                                "#T": "Time"
                                            },
                                            ExpressionAttributeValues: {
                                                ":Number": mybucket + '_' + fileKey.split('/')[0].slice(-1),
                                                ":d": getKey[0],
                                                ":t": getKey[1]
                                            }
                                        };


                                        docClient.query(para, function (err, data) {

                                            if (err) {
                                                console.error("Unable to Query the table. Error JSON:", JSON.stringify(err, null, 2));
                                            } else {
                                                if (data.Count == 0) {
                                                    eventtest();
                                                }
                                            }
                                        });
                                        
                                        function eventtest()
                                        {
                                            var para = {
                                                Bucket: mybucket,
                                                Key: fileKey,
                                            };
                                            
                                            s3.getObject(para, function (err5, data5) {
                                                
                                                if (err5) {
                                                    console.log(err5, err5.stack); 
    
                                                } else {
                                                    
                                                    var mycon = data5.Body.toString('ascii');           
                                                    
    
                                                    if (mycon.includes('\n')) {
                                                        var test = mycon.split('\n');
                                                        
                                                        var u = 0,
                                                            v = '',
                                                            params = '',
                                                            pr = '',
                                                            uu = uuidv1();
    
                                                        for (var n = 0; n < test.length - 1; n++) { //var n=0
                                                            var sample = test[n];                                                                    
                                                            
                                                            var rec = sample.split(' ');
                                                            
                                                            var dt = '';
                                                            for (var o = 9; o < rec.length; o++) {
                                                                dt += rec[o] + ' ';
                                                            }
                                                            
                                                            if (rec[9] != "undefined") {
                                                                var str = rec[9];
                                                                
                                                                if ((str.startsWith("EC"))) 
                                                                {
                                                                    var uu = uuidv1();
                                                                    
                                                                    var params = {
                                                                        TableName: "EventNew",
                                                                        Item: {
                                                                            "Date": getKey[0],
                                                                            "Time": getKey[1],
                                                                            "CDate": rec[4] + '-' + rec[5] + '-' + rec[6] + '-' + rec[7],
                                                                            "CTime": rec[3],
                                                                            "PumpID": mybucket + '_' + fileKey.split('/')[0].slice(-1),
                                                                            
                                                                            "EventKey": uu,
                                                                            "LastModified": n.LastModified,
                                                                            "Side": rec[1] + ' ' + rec[2],
                                                                            "DataSource": rec[8],
                                                                            "Description": dt,
                                                                        }
                                                                    };
                                                                    docClient.put(params, function (err, data) {
                                                                        if (err) {
                                                                            console.log("Unable to add item: " + "\n" + JSON.stringify(err, undefined, 2));
                                                                        } else {                                                                
                                                                            console.log("PutItem succeeded: " + "\n" + JSON.stringify(data, undefined, 2));
                                                                        }
                                                                    });
                                                                }
                                                            }
    
                                                        }
    
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });
    }

});
};