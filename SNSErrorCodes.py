import json
import sys
import logging
import pymysql
import os
from datetime import datetime
database = os.environ['database'];
rdshost = os.environ['rdshost'];
username = os.environ['username'];
password = os.environ['password'];

class Utils:
    def __init__(self):
        pass

    @staticmethod
    def parse_parameters(event):
            message = event['Records'][0]['Sns']['Message']
            subject = event['Records'][0]['Sns']['Subject']
            
            messagearray = message.split(' ')
            return_parameters = {}
            return_parameters['device_id'] = int(messagearray[0])
            return_parameters['side'] = messagearray[1]
            return_parameters['data_source'] = messagearray[8]
            return_parameters['code'] = messagearray[9].strip(':')
            
            #for datetime
            Date = messagearray[5]+' '+messagearray[6]+' '+messagearray[7]
            dateobject = datetime.strptime(Date,'%b %d %Y')
            newformat = dateobject.strftime('%Y-%m-%d')
            return_parameters['date'] = newformat+' '+messagearray[3]
            
            #for description
            description = ""
            length= len(messagearray)
            for i in range(10,length):
                description += messagearray[i]+' '
                if(i==length):
                    break
            return_parameters['description'] = description.strip(':')
                
            return {"parsedParams": return_parameters, "err": None}
        
class AuroraProvider:
    def __init__(self):

    
        try:
            self.conn = pymysql.connect(rdshost, user=username, passwd=password, db=database, connect_timeout=5)
        except:
            print("ERROR: Unexpected error: Could not connect to MySql instance.")
            sys.exit()

        print("SUCCESS: Connection to RDS mysql instance succeeded")
    
    def insert(self, result_params):
        print(result_params)
        with self.conn.cursor() as cur:
             
            cur.execute("""insert Error_Codes(device_id,side,event_date,data_source,code,description)
                values('%d','%s','%s','%s','%s','%s')""" % (result_params['device_id'],'all',result_params['date'],result_params['data_source'],result_params['code'],result_params['description']))
            
            self.conn.commit()
        

def lambda_handler(event, context):
    table_name = 'Error_Codes'
    repo=AuroraProvider()
    result_params = Utils.parse_parameters(event)
    repo.insert(result_params=result_params['parsedParams'])
    print('suceessfully inserted')
    return 'Inserted records'
