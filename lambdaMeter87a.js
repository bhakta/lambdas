var AWS = require('aws-sdk');
s3 = new AWS.S3({
    apiVersion: '2006-03-01'
});
var lambda = new AWS.Lambda();
var dynamodb = new AWS.DynamoDB({region:'us-east-2'});
var docClient = new AWS.DynamoDB.DocumentClient({region:'us-east-2'});
exports.handler = function(event,context) {
    const uuidv1 = require('uuid/v1');
uuidv1();
mybucket = '0013a2004155087a';
s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {
        Bucket: mybucket
    },
});
var files = [];
var ChildFolders = [];
var subSubFolders = [];
var subFolders = [];
var folders = [];
var mydate;
var mydevice = 'Device';
var myfolder = 'Meter Information';
s3.listObjects({
    Delimiter: '/'
}, function (err1, data1) {
    if (err1) {
        return console.log('There was an error listing your folders: ' + err1.message);
    } else {
        folders = data1.CommonPrefixes.map(function (commonPrefix) {
            var prefix = commonPrefix.Prefix;
            var folderName = decodeURIComponent(prefix.replace('/', ''));
            return folderName;
        });
        folders = folders.filter(function (e) {
            return e.indexOf(mydevice) !== -1;
        });
        
    }
    

    for(var j = 0; j < folders.length; j++)
    {
        
        s3.listObjects({
            Delimiter: '/',
            Prefix: folders[j] + '/'
        }, function (err2, data2) {
            if (err2) {
                return console.log('There was an error listing your subfolders: ' + err2.message);
            } else {
                subFolders = data2.CommonPrefixes.map(function (commonPrefix) {
                    var prefix1 = commonPrefix.Prefix;
                    var folderName1 = decodeURIComponent(prefix1.replace('', ''));
                    return folderName1;
                });
            }

           
            
            //for(var k = subFolders.length-1;k >= 0; k--)
            {
                var k = subFolders.length - 1;
               
                s3.listObjects({
                    Delimiter: '/',
                    Prefix: subFolders[k]
                }, function (err3, data3) {
                    if (err3) {
                        return console.log('There was an error listing your subsubfolders: ' + err3.message);
                    } else {
                        subSubFolders = data3.CommonPrefixes.map(function (commonPrefix) {
                            var prefix3 = commonPrefix.Prefix;
                            var folderName3 = decodeURIComponent(prefix3.replace('', ''));
                            return folderName3;
                        });

                        subSubFolders = subSubFolders.filter(function (e) {
                            return e.indexOf(myfolder) !== -1;
                        });
                    }
                   

                    //for(var l = 0;l < subSubFolders.length; l++)
                    {
                        var l = 0;
                        s3.listObjects({
                            Delimiter: '/',
                            Prefix: subSubFolders[l]
                        }, function (err4, data4) {
                            if (err4) {
                                return console.log('There was an error listing your childfolders: ' + err4.message);
                            } else {
                               
                                files = data4.Contents.map(function (photo) {
                                    var photoKey = photo.Key;
                                    
                                    return photoKey;
                                });
                            }
                            

                            for(var m=0; m < files.length; m++)
                            {
                                
                                var fileKey = files[m];

                                
                                if (fileKey != 'undefined' && fileKey != '' && fileKey != null) {
                                    
                                    if (fileKey.split('/')[2] == myfolder && fileKey.slice(-3) == 'txt') {
                                        var getKey = fileKey.slice(-23, -4).split(' ');

                                        
                                        var params = {
                                            Bucket: mybucket,
                                            Key: fileKey,
                                        };
                                        
                                        s3.getObject(params, function (err5, data5) {
                                            
                                            if (err5) {
                                                console.log(err5, err5.stack); 
                            
                                            } 
                                               else  {
                                                
                                                    var mycon = data5.Body.toString('ascii');
                                                    
                            
                                                
                            
                                                if(mycon.includes('\n'))
                                                    {
                                                        var test = mycon.split('\n');
                                                        
                                                        var loop = test.length / 9;
                                                        var k=1;
                                                        
                                                        var dt = '';
                                                        dt += '{';
                                                        for(var p=0; p<Math.round(loop)-1; p++)
                                                        {            
                                                            dt += '"'+test[k+3].slice(-1)+'":{';
                                                            dt += '"'+test[k+3].split(':')[0] + '":"' + test[k+3].split(':')[1]+'",';
                                                            dt += '"'+test[k+4].split(':')[0] + '":"' + test[k+4].split(':')[1]+'",';
                                                            dt += '"'+test[k+5].split(':')[0] + '":"' + test[k+5].split(':')[1]+'",';
                                                            dt += '"'+test[k+6].split(':')[0] + '":"' + test[k+6].split(':')[1]+'",';
                                                            dt += '"'+test[k+7].split(':')[0] + '":"' + test[k+7].split(':')[1]+'",';
                                                            dt += '"'+test[k+8].split(':')[0] + '":{';
                                                            dt += '"'+((test[k+9])?(test[k+9].split(':')[0]).trim():0) + '":"' + ((test[k+9].split(':')[1])?(test[k+9].split(':')[1]).trim():0)+'",';
                                                            dt += '"'+((test[k+10])?(test[k+10].split(':')[0]).trim():0) + '":"' + ((test[k+10].split(':')[1])?(test[k+10].split(':')[1]).trim():0)+'",';
                                                            dt += '"'+((test[k+11])?(test[k+11].split(':')[0]).trim():0) + '":"' + ((test[k+11].split(':')[1])?(test[k+11].split(':')[1]).trim():0)+'"';
                                                            dt += '}';
                                                            if(p == Math.round(loop)-2)
                                                                dt += '}';
                                                            else
                                                                dt += '},';
                                                            
                                                            k += 10;
                                                        }
                                                        dt += '}';
                                                        
                                                        var params = {
                                                            TableName :"Meter",
                                                            Item:{
                                                                "Date": getKey[0],
                                                                "Time": getKey[1],
                                                                "PumpID": mybucket+'_'+fileKey.split('/')[0].slice(-1),
                                                                "MeterKey": mybucket+'_'+fileKey.split('/')[0].slice(-1)+'_'+getKey[0]+'_'+getKey[1],
                                                                //"LastModified": n.LastModified,
                                                                "info": dt,
                                                                "ValveType":test[2].split(':')[1].trim()
                                                            
                                                            }
                                                        };
                                                        docClient.put(params, function(err, data) {
                                                            if (err) {
                                                               
                                                                console.log("Unable to add item: " + "\n" + JSON.stringify(err, undefined, 2));
                                                            } else {
                                                               
                                                                console.log("PutItem succeeded: " + "\n" + JSON.stringify(data, undefined, 2));
                                                            }
                                                        });
                                                    }
                                                }
                                        });
                                        
                                        
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });
    }

});

};