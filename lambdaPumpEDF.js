var AWS = require('aws-sdk');
s3 = new AWS.S3({
    apiVersion: '2006-03-01'
});
var dynamodb = new AWS.DynamoDB({region: 'us-east-2'});
var docClient = new AWS.DynamoDB.DocumentClient({region: 'us-east-2'});
exports.handler = function(event,context) {
const uuidv1 = require('uuid/v1');
uuidv1();
mybucket = '0013A200415B9EDF';
s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {
        Bucket: mybucket
    },
});
var files = [];
var ChildFolders = [];
var subSubFolders = [];
var subFolders = [];
var folders = [];
var mydate;
var mydevice = 'Device';
var myfolder = 'Pump Versions';
s3.listObjects({
    Delimiter: '/'
}, function (err1, data1) {
    if (err1) {
        return alert('There was an error listing your folders: ' + err1.message);
    } else {
        folders = data1.CommonPrefixes.map(function (commonPrefix) {
            var prefix = commonPrefix.Prefix;
            var folderName = decodeURIComponent(prefix.replace('/', ''));
            return folderName;
        });
        folders = folders.filter(function (e) {
            return e.indexOf(mydevice) !== -1;
        });
        
    }
    

    //for(var j = 0; j < folders.length; j++)
    {
      var j=1;  
        s3.listObjects({
            Delimiter: '/',
            Prefix: folders[j] + '/'
        }, function (err2, data2) {
            if (err2) {
                return alert('There was an error listing your subfolders: ' + err2.message);
            } else {
                subFolders = data2.CommonPrefixes.map(function (commonPrefix) {
                    var prefix1 = commonPrefix.Prefix;
                    var folderName1 = decodeURIComponent(prefix1.replace('', ''));
                    return folderName1;
                });
            }

           
            
            for(var k = subFolders.length-3;k >= 0; k--)
            {
                //var k = subFolders.length - 1;
               
                s3.listObjects({
                    Delimiter: '/',
                    Prefix: subFolders[k]
                }, function (err3, data3) {
                    if (err3) {
                        return alert('There was an error listing your subsubfolders: ' + err3.message);
                    } else {
                        subSubFolders = data3.CommonPrefixes.map(function (commonPrefix) {
                            var prefix3 = commonPrefix.Prefix;
                            var folderName3 = decodeURIComponent(prefix3.replace('', ''));
                            return folderName3;
                        });

                        subSubFolders = subSubFolders.filter(function (e) {
                            return e.indexOf(myfolder) !== -1;
                        });
                    }
                   

                    //for(var l = 0;l < subSubFolders.length; l++)
                    {
                        var l = 0;
                        s3.listObjects({
                            Delimiter: '/',
                            Prefix: subSubFolders[l]
                        }, function (err4, data4) {
                            if (err4) {
                                return alert('There was an error listing your childfolders: ' + err4.message);
                            } else {
                               
                                files = data4.Contents.map(function (photo) {
                                    var photoKey = photo.Key;
                                    
                                    return photoKey;
                                });
                            }
                            

                            for(var m=0; m < files.length; m++)
                            {
                                
                                var fileKey = files[m];
                                
                                
                                if (fileKey != 'undefined' && fileKey != '' && fileKey != null) {
                                    
                                    if (fileKey.split('/')[2] == myfolder && fileKey.slice(-3) == 'txt') {
                                        var getKey = fileKey.slice(-23, -4).split(' ');
                                        

                                        var params = {
                                            Bucket: mybucket,
                                            Key: fileKey,
                                        };
                                        
                                        s3.getObject(params, function (err5, data5) {
                                            
                                            if (err5) {
                                                console.log(err5, err5.stack); 
                            
                                            }
                                             else  {
                                            
                                                var mycon = data5.Body.toString('ascii');
                                            
                                            if(mycon.includes('\n'))
                                                {
                                                    var test = mycon.split('\n')
                                                    
                                                    var params = {
                                                        TableName :"Pump",
                                                        Item:{
                                                            "PumpID": mybucket+'_'+fileKey.split('/')[0].slice(-1),
                                                            "PumpKey": mybucket+'_'+fileKey.split('/')[0].slice(-1)+'_'+getKey[0]+'_'+getKey[1],
                                                            "Date": getKey[0],
                                                            "Time": getKey[1],
                                                            "PumpName": test[0],
                                                            "SoftwareVersion": test[1].split(' ')[7] + ' ' + test[1].split(' ')[8],
                                                            "SoftwareDate": test[2].split(' ')[10] + ' ' + test[2].split(' ')[11] + ' ' + test[2].split(' ')[12],
                                                            "HashCode": test[3].split(' ')[14],
                                                            "SoftwareCRC": test[4].split(' ')[11],
                                                            "MIPVersion": test[5].split(' ')[23] + ' ' + test[5].split(' ')[24],
                                                            "DoorNodeA": test[6].split(' ')[24] + ' ' + test[6].split(' ')[25],
                                                            "DoorNodeB": test[7].split(' ')[24] + ' ' + test[7].split(' ')[25],
                                                            "DoorNodeHostA": (!test[8].slice(13))?test[8].split(' ')[20]:0,
                                                            "DoorNodeHostB": (!test[9].slice(13))?test[9].split(' ')[20]:0,
                                                            "BootVersion": test[10].split(' ')[18] + ' ' + test[10].split(' ')[19],
                                                            "SiteID": mybucket
                                                        }
                                                    };
                                                    docClient.put(params, function(err, data) {
                                                        if (err) {
                                                            console.log("Unable to add item: " + "\n" + JSON.stringify(err, undefined, 2));
                                                        } else {
                                                            console.log("PutItem succeeded: " + "\n" + JSON.stringify(data, undefined, 2));
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                        
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });
    }

});
};


