import json
import boto3

message = '12 All Pump 00:01:00 Sat Jan 01 2000 eDataBase EC5239:MINOR:Database init forced'
client = boto3.client('sns')
arn="arn:aws:sns:us-east-1:332167152351:errorcodes-sns-topic"

def lambda_handler(event, context):
    
    response = client.publish(
    TargetArn=arn,
    Message=message,
    Subject='a short subject for your message'
    )
    return response
