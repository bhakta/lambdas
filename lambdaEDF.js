var AWS = require('aws-sdk');
var lambda = new AWS.Lambda();
exports.handler = (event, context, callback) => {
    var paramEvent = {
    FunctionName: 'lambdaEventEDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "Test" }'
  };
  //lambda.invoke(paramEvent, function(err, data) {
  //  if (err) {
  //    context.fail(err);
  //  } else {
  //    context.succeed('Event said '+ data.Payload);
  //  }
  //});
    
    var paramConf = {
    FunctionName: 'lambdaConfigurationEDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "Test" }'
  };
  lambda.invoke(paramConf, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Configuration said '+ data.Payload);
    }
  });
  var paramEveNew = {
    FunctionName: 'lambdaallevents1', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "bucket" : "'+ event.Records[0].s3.bucket.name +'", "key" : "'+event.Records[0].s3.object.key+'" }'
  };

  lambda.invoke(paramEveNew, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Event New said '+ data.Payload);
    }
  });
  
  var paramWegNew = {
    FunctionName: 'lambdaallweights1', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "bucket" : "'+ event.Records[0].s3.bucket.name +'", "key" : "'+event.Records[0].s3.object.key+'" }'
  };

  lambda.invoke(paramWegNew, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Weights New said '+ data.Payload);
    }
  });
  
  var paramPump = {
    FunctionName: 'lambdaPumpEDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "Test" }'
  };

  lambda.invoke(paramPump, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Pump said '+ data.Payload);
    }
  });
  
  var paramPerf1Log = {
    FunctionName: 'lambdaPerformance1LogEDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "Alex" }'
  };

  lambda.invoke(paramPerf1Log, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Performance1 Log said '+ data.Payload);
    }
  });
  
  var paramPerf2 = {
    FunctionName: 'lambdaPerformance2EDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "Alex" }'
  };

  lambda.invoke(paramPerf2, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Performance 2 said '+ data.Payload);
    }
  });
  
  var paramMet = {
    FunctionName: 'lambdaMeterEDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "Alex" }'
  };

  lambda.invoke(paramMet, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Meter said '+ data.Payload);
    }
  });
  
  var paramConfLog = {
    FunctionName: 'lambdaConfigurationLogEDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "test" }'
  };

  lambda.invoke(paramConfLog, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Configuration Log said '+ data.Payload);
    }
  });
  
   var paramPerf2Log = {
    FunctionName: 'lambdaPerformance2LogEDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "Alex" }'
  };

  lambda.invoke(paramPerf2Log, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Performance 2 Log said '+ data.Payload);
    }
  });
  
  var paramMetLog = {
    FunctionName: 'lambdaMeterLogEDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "Alex" }'
  };

  lambda.invoke(paramMetLog, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Meter Log said '+ data.Payload);
    }
  });
  
  var paramPerf1 = {
    FunctionName: 'lambdaPerformance1EDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "Performance" }'
  };

  lambda.invoke(paramPerf1, function(err, data) {
    if (err) {
      context.fail(err);
    } else {
      context.succeed('Performance 1 said '+ data.Payload);
    }
  });
  
  var paramWeig = {
    FunctionName: 'lambdaWeightsEDF', 
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{ "name" : "Alex" }'
  };

  //lambda.invoke(paramWeig, function(err, data) {
  //  if (err) {
  //    context.fail(err);
  //  } else {
  //    context.succeed('Weights said '+ data.Payload);
  //  }
  //});

};